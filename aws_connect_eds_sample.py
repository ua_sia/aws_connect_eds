from ldap3 import Server, Connection, ALL
import json

# EDS application account and password...you would get these from environment vars or SSM
eds_username = "fakeaccount"
eds_password = "fakepassword"

# base DN for search and DN construction
base_dn = "dc=eds,dc=arizona,dc=edu"

# LDAP search filter
search_filter = "(&(edupersonaffiliation=employee)(employeephone=%s))"

# phone number to search one...this would be provided in input to Lambda handler
phone = "5206265981"

# connect and bind to LDAP server
server = Server("ldaps://eds.iam.arizona.edu", get_info=ALL)
conn = Connection(
    server,
    "uid=%s,ou=app users,%s" % (eds_username, base_dn),
    eds_password,
    return_empty_attributes=False,
    auto_bind=True
)

# perform search operation
results = conn.search("ou=people,%s" % base_dn, search_filter % phone, attributes=['uid','mail','cn'])
if len(conn.entries) > 0:
    # found one or more entries...grab first one
    e = json.loads(conn.entries[0].entry_to_json())['attributes']
    # get attributes ... ldap3 returns all attributes as arrays, so process accordingly
    cn = e['cn'][0]
    netid = e.get('uid', ["n/a"])[0]
    mail = e.get('mail', ["n/a"])[0]
    print("Match for %s: name = %s, netid = %s, email = %s" % (phone, cn, netid, mail))
else:
    print("No match for phone = %s", phone)
